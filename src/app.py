'''
    This file contains the implementation of the REST api.
    @author Swarna Shenoy (11/17/2019)
'''

from flask import Flask, jsonify
from flask import make_response
from werkzeug.routing import IntegerConverter
from src.db import create_connection, insert_db, query_db_values

# To handle negative values in Flask.
class SignedIntConverter(IntegerConverter):
   regex = r"-?\d+"

app = Flask(__name__)
app.url_map.converters['signed_int'] = SignedIntConverter

# Path to the db file.
DATABASE=r".\db\fib.db"

# REST API that would generate fibonacci sequence using HTTP GET.
@app.route('/api/fib/<signed_int:num>', methods=['GET'])
def get_fib(num):
    fib = []

    # If the number given is negative, notify the user.
    if num <= 0:
        return make_response(jsonify({'error': 'Please enter a number greater than 0.'}), 404)

    try:
        # Open connection to DB.
        conn = create_connection(DATABASE)

        # Get the sequence present in the DB.
        fib = query_db_values(conn, num)
        length = len(fib)

        # If the values are insufficient, generate the rest and add to DB.
        for i in range(length, num):
            fib.append(fib[i-1] + fib[i-2])
            insert_db(conn, i + 1, fib[i])
        
        conn.close()
    except:
        return make_response(jsonify({'error': 'Internal Server Error.'}), 500)

    return jsonify({'sequence': fib})

@app.route('/')
def index():
    return "Fibonacci Sequence"

if __name__ == '__main__':
    app.run(debug=False)