'''
    This file contains the implementation of database routines.
    @author Swarna Shenoy (11/17/2019)
'''

import sqlite3

def create_connection(db_file):
    '''
        Creates a connection to the database.
        @param db_file - Path to the database file.
    '''
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except:
        print("Error while creating connection.")
 
    return conn

# To create table fib_data for the first time.
def create_table(conn, create_table_sql):
    '''
        Creates a table in the database
        @param conn - Connection to the database.
        @param create_table_sql - SQL to create the table.
    '''
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except:
        print("Error")

# To return all values present in fib_data upto num'th index.
def query_db_values(conn, num):
    '''
        Queries the values for a given number.
        @param conn - Connection to the database.
        @param num - The primary key to query.
    '''
    try:
        cur = conn.cursor()
        cur.execute("SELECT value from fib_data where num <= {0}".format(num))
        return [row[0] for row in cur.fetchall()]
    except:
        return None

def list_db(conn):
    '''
        Prints all the values in the database.
        @param conn - Connection to the database.
    '''
    try:
        c = conn.cursor()
        c.execute("SELECT value from fib_data;")
        rows = c.fetchall()
        for r in rows:
            print(r)
    except:
        print("Error")

def insert_db(conn, num, value):
    '''
        Inserts a new value into the database
        @param conn - Connection to the database.
        @param num - A primary key.
        @param value - The value corresponding to the primary key.
    '''
    try:
        c = conn.cursor()
        c.execute("INSERT INTO fib_data VALUES({}, {});".format(num, value))
        conn.commit()
    except:
        print("Error while inserting.")

if __name__ == '__main__':
    database = r".\db\fib.db"
    
    sql_create_table = '''CREATE TABLE IF NOT EXISTS fib_data (
        num integer PRIMARY KEY,
        value UNSIGNED BIG INT
        );'''
    
    conn = create_connection(database)
    if conn is not None:
        create_table(conn, sql_create_table) 

        # Uncomment following 2 lines for the first time to enter values for 1 and 2.
        #insert_db(conn, 1, 0)
        #insert_db(conn, 2, 1)
        list_db(conn)
        conn.close()
    else:
        print("Error! cannot create the database connection.")
