Git Clone: git clone https://swarnashenoy@bitbucket.org/swarnashenoy/fibonacci_rest.git

Top level directory structure:
fib\
	db\
	env\
	src\
	tests\


Now, in the command prompt go to the 'fib' directory.

Enter the following command: env\Scripts\activate

Run the Flask application: python src\app.py

Open the browser and then type following URI: http://127.0.0.1:5000/api/fib/5

In the above URI:  "http://127.0.0.1:5000/api/fib/" is the URL and "5" is the resource/identifier (length of the sequence).

Change the value to anything other than 5 to test the Fibonacci Sequence.

