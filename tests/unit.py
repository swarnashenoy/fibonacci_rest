'''
    This file contains unit test for the REST api.
    @author Swarna Shenoy (11/17/2019)
'''

from src.app import app
import unittest

class FibUnitTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass
    
    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True 
    
    #Test if the URI is correct
    def test_home(self):
        result = self.app.get('/')
        self.assertEqual(result.data, b'Fibonacci Sequence')
    
    #Test if the user has entered a negative number.
    def test_negetive_number(self):
        result = self.app.get('/api/fib/-3')
        self.assertIn('{"error":"Please enter a number greater than 0."}', result.data.decode('ASCII'))

    #Test the sequence for a positive number 5.
    def test_positive_number(self):
        result = self.app.get('/api/fib/5')
        self.assertIn('[0,1,1,2,3]', result.data.decode('ASCII'))
    
    #Test if any invalid input other than numbers are entered.
    def test_invalid_url(self):
        result = self.app.get('/api/fib/abc')
        self.assertIn('The requested URL was not found on the server', result.data.decode('ASCII'))

if __name__ == '__main__':
    unittest.main()
